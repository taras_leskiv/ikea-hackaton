# Pavlo Bazilinskyy <pavlo.bazilinskyy@gmail.com>

import time, sys, signal, atexit
import pyupm_groveloudness as groveSound
import pyupm_grove as grove
import pyupm_buzzer as upmBuzzer
import mraa
import pyupm_i2clcd as lcd
import pyupm_ldt0028 as ldt0028


## Sensors
tempSensor = grove.GroveTemp(0)
loudnessSensor = groveSound.GroveLoudness(1)
buttonSensor = grove.GroveButton(2)
buttonSensor2 = grove.GroveButton(3)
vibrationSensor = ldt0028.LDT0028(3)

## Screen
screen = lcd.Jhd1313m1(0, 0x3E, 0x62)
screen.setCursor(0,0)
screen.setColor(0, 0, 255)

## Buzzer
chords = [upmBuzzer.DO, upmBuzzer.RE, upmBuzzer.MI, upmBuzzer.FA, upmBuzzer.SOL, upmBuzzer.LA, upmBuzzer.SI, upmBuzzer.DO, upmBuzzer.SI];
soundPlayed = 0
tempSoundPlayed = 0
buzzer = upmBuzzer.Buzzer(6)

## Lights
led1 = grove.GroveLed(7)
led2 = grove.GroveLed(8)
toilet1Occupied = 0
toilet2Occupied = 0

# Read sensor data
while 1==1:
	temp = tempSensor.value()
	loudness  = loudnessSensor.value()
	button1 = buttonSensor.value()
	button2 = buttonSensor2.value()
	vibration = vibrationSensor.getSample()
	f = open('sensor.json','w')
	f.write("{ \"temp\" : %d, \"sound\" : %d, \"button1\" : %d,  \"button2\" : %d}" % (temp, loudness, button1, button2))
	print "{ \"temp\" : %d, \"sound\" : %d, \"button1\" : %d, \"button2\" : %d}" % (temp, loudness, button1, button2)
	f.close() # you can omit in most cases as the destructor will call it

	#  Coffee
	if temp >= 35:
		screen.setColor(255, 0, 0)
		screen.setCursor(0,0)
		screen.write("Busy...")
		screen.setCursor(1,0)
		screen.write("Temp: %d C    " % temp)

		Play sound (DO, RE, MI, etc.), pausing for 0.1 seconds between notes
		for chord_ind in range (0, 7):
			# play each note for one second
			print buzzer.playSound(chords[chord_ind], 1000000)
			time.sleep(0.1)
		if soundPlayed == 0:
			buzzer.playSound(chords[3], 1000000)
			buzzer.playSound(chords[4], 1000000)
			soundPlayed = 1
			tempSoundPlayed = temp
			del buzzer
	else:
		screen.setColor(0, 255, 0)
		screen.setCursor(0,0)
		screen.write("FOKUS                  ")
		screen.setCursor(1,0)
		screen.write("Temp: %d C    " % temp)
		if temp <= tempSoundPlayed - 5: # Detect if temperature dropped by more than 5 deg
			soundPlayed = 0

	# Indicator of toilet 1
	if button1 == 1:
		led1.on()
	else:
		led1.off()

	# Indicator of toilet 2
	if button2 == 1:
		led2.on()
	else:
		led2.off()

	time.sleep(.1)

# Delete sensor objects
del tempSensor
del loudnessSensor
del buttonSensor
del buttonSensor2
del vibrationSensor
del screen
del led1
del led2
del buzzer