﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OfficeTemp : MonoBehaviour
{
    public Communication comm;
    public Text text;

    void Awake()
    {
        text.text = "T 23°C";
    }

    void Update()
    {
        text.text = "T " + comm.temepetatureOffice + "°C";
    }
}
