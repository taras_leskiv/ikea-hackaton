﻿using UnityEngine;
using System.Collections;

public class MapBeacons : MonoBehaviour
{
    public Communication comm;

    public GameObject john;
    public GameObject mary;    

    public GameObject johnAway;
    public GameObject maryAway;

    void Update()
    {
        if (comm.isJohnInMeetingRoom)
        {
            john.SetActive(true);
            johnAway.SetActive(false);
        }
        else
        {
            john.SetActive(false);
            johnAway.SetActive(true);
        }

        if (comm.isMaryInMeetingRoom)
        {
            mary.SetActive(true);
            maryAway.SetActive(false);
        }
        else
        {
            mary.SetActive(false);
            maryAway.SetActive(true);
        }
    }

}
