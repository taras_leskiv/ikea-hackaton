﻿using UnityEngine;
using System.Collections;

public class CoffeeMachine : MonoBehaviour
{

    public int temperatureThreshold = 40;
    public Communication comm;  

    private Animator animator;

    public bool IsCoffeeBrewing
    {
        get {
            int temp;
            int.TryParse(comm.temepetature, out temp);
            return temp > temperatureThreshold;
        }
    }

    void Awake()
    {
        animator = GetComponent<Animator>();
        animator.SetBool("free", true);
    }

    void Update()
    {
        int temp;
        int.TryParse(comm.temepetature, out temp);
        if (temp > temperatureThreshold)
        {
            animator.SetBool("free", false);
        }
        else
        {
            animator.SetBool("free", true);
        }
    }
}
