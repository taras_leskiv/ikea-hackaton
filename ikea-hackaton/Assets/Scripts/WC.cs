﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WC : MonoBehaviour
{
    public Communication comm;

    public Image wc1;
    public Image wc2;
    public Toggle wcToggle;

    public Sprite free;
    public Sprite busy;

    void Update()
    {
        if (wcToggle.isOn)
        {
            wc1.gameObject.SetActive(true);
            wc2.gameObject.SetActive(true);
        }
        else
        {
            wc1.gameObject.SetActive(false);
            wc2.gameObject.SetActive(false);
        }

        if (comm.isBtn1Pressed == "1")
        {
            wc1.sprite = busy;
        }
        else
        {
            wc1.sprite = free;
        }

        if (comm.isBtn2Pressed == "1")
        {
            wc2.sprite = busy;
        }
        else
        {
            wc2.sprite = free;
        }
    }
}
