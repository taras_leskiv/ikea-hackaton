﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MapNoise : MonoBehaviour
{
    public Communication comm;
    public float maxNoise = 800;
    public Slider slider;

    void Update()
    {
        float sound = int.Parse(comm.sound);
        slider.value = sound;
    }
}
