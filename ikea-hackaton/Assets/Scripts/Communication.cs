﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class Communication : MonoBehaviour
{
    private const string BeaconDoorJohn = "1fdeff6258bba37a";
    private const string BeaconBycicleMary = "49b55e37ca533f56";
    private const string BeaconFridgePeter = "9bf2295d7261142d";

    public string temepetature = string.Empty;
    public string temepetatureOffice = string.Empty;


    public string sound = string.Empty;
    public string vibration = string.Empty;

    public string isBtn1Pressed = string.Empty;
    public string isBtn2Pressed = string.Empty;

    public bool isJohnInMeetingRoom = false;
    public bool isMaryInMeetingRoom = false;

    string simulatedOfficeData;
    UiController ui;

    void Awake()
    {
        ui = GetComponent<UiController>();
    }

    void Start()
    {
        StartCoroutine(UpdateData());
        StartCoroutine(UpdateSensorData());
        StartCoroutine(UpdateBeaconsData());
    }

    IEnumerator UpdateData()
    {
        while (true)
        {
            WWW www = new WWW("https://www.dropbox.com/s/98eq9fzmqeav7y3/db.json?dl=1\t"); 
            yield return www;
            simulatedOfficeData = www.text;
//            Debug.Log(www.text);
            ParseData(www.text);

            yield return new WaitForSeconds(3.0f);
        }
    }

    private void ParseData(string simulationData)
    {
        try
        {
            var json = new JSONObject(simulationData);
            if (json != null)
            {
                temepetatureOffice = json.GetField("Temp").ToString();
                Debug.Log(temepetatureOffice);
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    IEnumerator UpdateSensorData()
    {
        while (true)
        {
            var desktopPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
            var sensorDataPath = Path.Combine(desktopPath, "sensor.json");
//            Debug.Log(sensorDataPath);
            using (StreamReader sr = new StreamReader(sensorDataPath))
            {
                // Read the stream to a string, and write the string to the console.
                string line = sr.ReadToEnd();
                Debug.Log(line);
                ParseSensorData(line);
            }
            
            yield return new WaitForSeconds(0.2f);
        }
    }

    private void ParseSensorData(string data)
    {
        try
        {
            var json = new JSONObject(data);
            if (json != null)
            {
                temepetature = json.GetField("temp").ToString();
                sound = json.GetField("sound").ToString();

                isBtn1Pressed = json.GetField("button1").ToString();
                isBtn2Pressed = json.GetField("button2").ToString();

                ui.tempText.text = "T is " + temepetature;
                ui.soundText.text = "S is " + sound;
                ui.btn1Text.text = isBtn1Pressed == "0" ? "FREE" : "OCCUPIED";
                ui.btn2Text.text = isBtn2Pressed == "0" ? "FREE" : "OCCUPIED";
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }


    IEnumerator UpdateBeaconsData()
    {
        while (true)
        {
            var desktopPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
            var sensorDataPath = Path.Combine(desktopPath, "tags.txt");
            using (StreamReader sr = new StreamReader(sensorDataPath))
            {
                // Read the stream to a string, and write the string to the console.
                string line = sr.ReadToEnd();
//                Debug.Log(line);
                ParseTagsData(line);
            }

            yield return new WaitForSeconds(1f);
        }
    }

    private void ParseTagsData(string data)
    {
        try
        {
            var json = new JSONObject(data);
            if (json != null)
            {
//                Debug.Log(json.ToString());
                isJohnInMeetingRoom = json.HasField(BeaconDoorJohn);
                isMaryInMeetingRoom = json.HasField(BeaconBycicleMary);
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }
}
