﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MapCoffee : MonoBehaviour
{
    public Communication comm;
    public CoffeeMachine coffeeMachine;

    public Image coffee;
    public Toggle wcToggle;

    public Sprite free;
    public Sprite busy;


    void Update()
    {
        if (wcToggle.isOn)
        {
            coffee.gameObject.SetActive(true);
        }
        else
        {
            coffee.gameObject.SetActive(false);
        }

        if (coffeeMachine.IsCoffeeBrewing)
        {
            coffee.sprite = busy;
        }
        else
        {
            coffee.sprite = free;
        }
    }
}
