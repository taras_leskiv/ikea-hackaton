﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

namespace Ikea.NotificationCenter
{
    class Program
    {
        private static int counter = 0;
        private static int status = 0;

        static void Main(string[] args)
        {
            var filePath = @"C:\Users\myroslava\Dropbox\db.json";

            //update every 5 seconds
            while (true)
            {
                status = counter % 10;

                var settings = new Settings
                {
                    Toilet1Busy = GetToilet1Stat(),
                    Toilet2Busy = GetToilet2Stat(),
                    Toilet3Busy = GetToilet3Stat(),
                    Toilet4Busy = GetToilet4Stat(),
                    CoffieMachine1Temp = GetCoffee1Temp(),
                    CoffieMachine2Temp = GetCoffee2Temp(),
                    CoffieMachine3Temp = GetCoffee3Temp(),
                    EmployeeStatus = EmployeeStatus(),
                    Colleague1Status = Colleague1Status(),
                    Colleague2Status = Colleague2Status(),
                    Colleague3Status = Colleague3Status(),
                    Colleague4Status = Colleague4Status(),
                    Colleague5Status = Colleague5Status(),
                    Colleague6Status = Colleague6Status(),
                    Colleague7Status = Colleague7Status(),
                    Colleague8Status = Colleague8Status(),
                    Printer1Busy = GetPrinter1Busy(),
                    Printer2Busy = GetPrinter2Busy(),
                    Printer3Busy = GetPrinter3Busy(),
                    Basecamp = GetBasecamp(),
                    HighFocused = GetHighFocused(),
                    VoiceCall = GetVoiceCall(),
                    OpenCollaboration = GetOpenCollaboration(),
                    MeetingRoom1 = GetMeetingRoom1(),
                    MeetingRoom2 = GetMeetingRoom2(),
                    MeetingRoom3 = GetMeetingRoom3(),
                    MeetingRoom4 = GetMeetingRoom4(),
                    MeetingRoom5 = GetMeetingRoom5(),
                    Canteen = GetCanteen(),
                    BasecampTemp = BasecampTemp(),
                    HighFocusedTemp = HighFocusedTemp(),
                    VoiceCallTemp = VoiceCallTemp(),
                    OpenCollaborationTemp = OpenCollaborationTemp(),
                    MeetingRoom1Temp = MeetingRoom1Temp(),
                    MeetingRoom2Temp = MeetingRoom2Temp(),
                    MeetingRoom3Temp = MeetingRoom3Temp(),
                    MeetingRoom4Temp = MeetingRoom4Temp(),
                    MeetingRoom5Temp = MeetingRoom4Temp(),
                    CanteenTemp = CanteenTemp()
                };

                Console.WriteLine("{0}  {1}  {2}  {3}  {4}  {5}  {6}  {7}  {8}  {9}  {10}  {11}  {12}  {13}  {14}  {15}  {16}  {17}  {18} " +
                                  "ROOMS {19}  {20}  {21} {22}  {23}  {24}  {25}  {26}  {27}  {28} " +
                                  "ROOMS TEMP  {29}  {30}  {31} {32}  {33}  {34}  {35}  {36}  {37}  {38}",
                    settings.Toilet1Busy,
                    settings.Toilet2Busy,
                    settings.Toilet3Busy,
                    settings.Toilet4Busy,
                    settings.CoffieMachine1Temp,
                    settings.CoffieMachine2Temp,
                    settings.CoffieMachine3Temp,
                    settings.EmployeeStatus,
                    settings.Colleague1Status,
                    settings.Colleague2Status,
                    settings.Colleague3Status,
                    settings.Colleague4Status,
                    settings.Colleague5Status,
                    settings.Colleague6Status,
                    settings.Colleague7Status,
                    settings.Colleague8Status,
                    settings.Printer1Busy,
                    settings.Printer2Busy,
                    settings.Printer3Busy,
                    settings.Basecamp,
                    settings.HighFocused,
                    settings.VoiceCall,
                    settings.OpenCollaboration,
                    settings.MeetingRoom1,
                    settings.MeetingRoom2,
                    settings.MeetingRoom3,
                    settings.MeetingRoom4,
                    settings.MeetingRoom5,
                    settings.Canteen,
                    settings.BasecampTemp,
                    settings.HighFocusedTemp,
                    settings.VoiceCallTemp,
                    settings.OpenCollaborationTemp,
                    settings.MeetingRoom1Temp,
                    settings.MeetingRoom2Temp,
                    settings.MeetingRoom3Temp,
                    settings.MeetingRoom4Temp,
                    settings.MeetingRoom5Temp,
                    settings.CanteenTemp
                    );

                SerializeJson(settings, filePath);
                Thread.Sleep(5000);

                counter++;
            }
        }

        private static void SerializeJson(Settings settings, string filePath)
        {
            string jsonString = JsonConvert.SerializeObject(settings, Formatting.Indented);
            File.WriteAllText(filePath, jsonString);
        }

        private static void DeserializeJson(string filePath)
        {
            FileInfo file = new FileInfo(filePath);
            Dictionary<string, string> items;

            using (StreamReader r = new StreamReader(filePath))
            {
                string json = r.ReadToEnd();
                items = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            }
        }

        private static bool GetToilet1Stat()
        {
            var result = false;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    result = true;
                    break;
                case 6:
                case 7:
                case 8:
                case 9:
                    result = false;
                    break;
            }

            return result;
        }

        private static bool GetToilet2Stat()
        {
            return true;
        }

        private static bool GetToilet3Stat()
        {
            var result = false;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                    result = false;
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    result = true;
                    break;
            }

            return result;
        }

        private static bool GetToilet4Stat()
        {
            var result = false;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    result = false;
                    break;
                case 6:
                case 7:
                case 8:
                case 9:
                    result = true;
                    break;
            }

            return result;
        }

        private static int GetCoffee1Temp()
        {
            var result = 20;

            switch (status)
            {
                case 0:
                case 1:
                    result = 60;
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                    result = 20;
                    break;
                case 6:
                case 7:
                    result = 60;
                    break;
                case 8:
                case 9:
                    result = 20;
                    break;
            }

            return result;
        }

        private static int GetCoffee2Temp()
        {
            var result = 20;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    result = 60;
                    break;
                case 7:
                case 8:
                case 9:
                    result = 20;
                    break;
            }

            return result;
        }

        private static int GetCoffee3Temp()
        {
            var result = 20;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    result = 20;
                    break;
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    result = 60;
                    break;
            }

            return result;
        }

        private static string EmployeeStatus()
        {
            return "workFromHome";
        }

        private static string Colleague1Status()
        {
            return "vacation";
        }

        private static string Colleague2Status()
        {
            return "workFromOffice";
        }

        private static string Colleague3Status()
        {
            return "workFromOffice";
        }

        private static string Colleague4Status()
        {
            return "workFromOffice";
        }

        private static string Colleague5Status()
        {
            return "workFromHome";
        }

        private static string Colleague6Status()
        {
            return "workFromOffice";
        }

        private static string Colleague7Status()
        {
            return "workFromOffice";
        }

        private static string Colleague8Status()
        {
            return "vacation";
        }

        private static bool GetPrinter1Busy()
        {
            var result = false;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    result = false;
                    break;
                case 6:
                case 7:
                    result = true;
                    break;
                case 8:
                case 9:
                    result = false;
                    break;
            }

            return result;
        }

        private static bool GetPrinter2Busy()
        {
            var result = false;

            switch (status)
            {
                case 0:
                case 1:
                    result = false;
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                    result = true;
                    break;
                case 6:
                case 7:
                case 8:
                case 9:
                    result = false;
                    break;
            }

            return result;
        }

        private static bool GetPrinter3Busy()
        {
            return true;
        }

        private static int GetBasecamp()
        {
            var result = 0;

            switch (status)
            {
                case 0:
                case 1:
                    result = 53;
                    break;
                case 2:
                case 3:
                    result = 52;
                    break;
                case 4:
                case 5:
                case 6:
                    result = 48;
                    break;
                case 7:
                case 8:
                case 9:
                    result = 34;
                    break;
            }

            return result;
        }

        private static int GetHighFocused()
        {
            var result = 0;

            switch (status)
            {
                case 0:
                case 1:
                    result = 19;
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    result = 13;
                    break;
            }

            return result;
        }

        private static int GetVoiceCall()
        {
            return 12;
        }

        private static int GetOpenCollaboration()
        {
            return 11;
        }

        private static int GetMeetingRoom1()
        {
            var result = 0;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    result = 0;
                    break;
                case 7:
                case 8:
                case 9:
                    result = 14;
                    break;
            }

            return result;
        }

        private static int GetMeetingRoom2()
        {
            var result = 0;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                    result = 5;
                    break;
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    result = 0;
                    break;
            }

            return result;
        }

        private static int GetMeetingRoom3()
        {
            var result = 0;

            switch (status)
            {
                case 0:
                case 1:
                    result = 0;
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    result = 7;
                    break;
            }

            return result;
        }

        private static int GetMeetingRoom4()
        {
            return 0;
        }

        private static int GetMeetingRoom5()
        {
            return 0;
        }

        private static int GetCanteen()
        {
            var result = 0;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                    result = 0;
                    break;
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    result = 9;
                    break;
            }

            return result;
        }

        private static int BasecampTemp()
        {
            var result = 20;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    result = 23;
                    break;
                case 7:
                case 8:
                case 9:
                    result = 22;
                    break;
            }

            return result;
        }

        private static int HighFocusedTemp()
        {
            var result = 20;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                    result = 22;
                    break;
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    result = 21;
                    break;
            }

            return result;
        }

        private static int VoiceCallTemp()
        {
            return 21;
        }

        private static int OpenCollaborationTemp()
        {
            var result = 20;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    result = 22;
                    break;
                case 7:
                case 8:
                case 9:
                    result = 23;
                    break;
            }

            return result;
        }

        private static int MeetingRoom1Temp()
        {
            var result = 20;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    result = 22;
                    break;
                case 7:
                case 8:
                case 9:
                    result = 24;
                    break;
            }

            return result;
        }

        private static int MeetingRoom2Temp()
        {
            var result = 20;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                    result = 24;
                    break;
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    result = 23;
                    break;
            }

            return result;
        }

        private static int MeetingRoom3Temp()
        {
            var result = 20;

            switch (status)
            {
                case 0:
                case 1:
                    result = 19;
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    result = 21;
                    break;
            }

            return result;
        }

        private static int MeetingRoom4Temp()
        {
            return 24;
        }

        private static int MeetingRoom5Temp()
        {
            return 19;
        }

        private static int CanteenTemp()
        {
            var result = 20;

            switch (status)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                     result = 23;
                    break;
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    result = 25;
                    break;
            }

            return result;
        }

        //THIS PART WAS USED AT THE DEMO!

        //private static int counter = 0;
        //private static int status = 0;

        //static void Main(string[] args)
        //{
        //    var filePath = @"C:\Users\myroslava\Dropbox\db.json";

        //    //update every 5 seconds
        //    while (true)
        //    {
        //        status = counter % 2;

        //        var settings = new Settings
        //        {
        //            Temp = GetBasecampTemp()
        //        };

        //        Console.WriteLine(settings.Temp);

        //        SerializeJson(settings, filePath);
        //        Thread.Sleep(5000);

        //        counter++;
        //    }
        //}

        //private static void SerializeJson(Settings settings, string filePath)
        //{
        //    string jsonString = JsonConvert.SerializeObject(settings, Formatting.Indented);
        //    File.WriteAllText(filePath, jsonString);
        //}

        //private static void DeserializeJson(string filePath)
        //{
        //    FileInfo file = new FileInfo(filePath);
        //    Dictionary<string, string> items;

        //    using (StreamReader r = new StreamReader(filePath))
        //    {
        //        string json = r.ReadToEnd();
        //        items = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        //    }
        //}

        //private static int GetBasecampTemp()
        //{
        //    var result = 20;

        //    switch (status)
        //    {
        //        case 0:
        //            result = 24;
        //            break;
        //        case 1:
        //            result = 22;
        //            break;
        //    }

        //    return result;
        //}
    }
}
