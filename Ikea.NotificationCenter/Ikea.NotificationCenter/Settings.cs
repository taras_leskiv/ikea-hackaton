﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ikea.NotificationCenter
{
    public class Settings
    {
        public bool Toilet1Busy { get; set; }
        public bool Toilet2Busy { get; set; }
        public bool Toilet3Busy { get; set; }
        public bool Toilet4Busy { get; set; }
        public int CoffieMachine1Temp { get; set; }
        public int CoffieMachine2Temp { get; set; }
        public int CoffieMachine3Temp { get; set; }
        public string EmployeeStatus { get; set; }
        public string Colleague1Status { get; set; }
        public string Colleague2Status { get; set; }
        public string Colleague3Status { get; set; }
        public string Colleague4Status { get; set; }
        public string Colleague5Status { get; set; }
        public string Colleague6Status { get; set; }
        public string Colleague7Status { get; set; }
        public string Colleague8Status { get; set; }
        public bool Printer1Busy { get; set; }
        public bool Printer2Busy { get; set; }
        public bool Printer3Busy { get; set; }

        public int Basecamp { get; set; }
        public int HighFocused { get; set; }
        public int VoiceCall { get; set; }
        public int OpenCollaboration { get; set; }
        public int MeetingRoom1 { get; set; }
        public int MeetingRoom2 { get; set; }
        public int MeetingRoom3 { get; set; }
        public int MeetingRoom4 { get; set; }
        public int MeetingRoom5 { get; set; }
        public int Canteen { get; set; }

        public int BasecampTemp { get; set; }
        public int HighFocusedTemp { get; set; }
        public int VoiceCallTemp { get; set; }
        public int OpenCollaborationTemp { get; set; }
        public int MeetingRoom1Temp { get; set; }
        public int MeetingRoom2Temp { get; set; }
        public int MeetingRoom3Temp { get; set; }
        public int MeetingRoom4Temp { get; set; }
        public int MeetingRoom5Temp { get; set; }
        public int CanteenTemp { get; set; }
    }

    //THIS PART WAS USED AT THE DEMO!

    //public class Settings
    //{
    //    public int Temp { get; set; }
    //}
}
